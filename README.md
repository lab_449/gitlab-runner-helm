# gitlab-runner-helm

This repositoy can help you run gitlab runner in Kubernetes (mk8s).

https://docs.gitlab.com/runner/install/kubernetes.html


## oh-my-zsh (Optional)
 
Add plugins for oh-my-zsh for enable autocompletion for kubectl or microk8s

    # somewhere in your zshrc
    plugins=(... kubectl)
    plugins=(... microk8s)

#

Add the GitLab Helm repository:

    microk8s.helm3 repo add gitlab https://charts.gitlab.io

Create custom namaspace

    microk8s.kubectl create namespace gitlab-runner

<!-- Create imagePullSecret in gitlab as follow in: 

    microk8s.kubectl create secret docker-registry gitlab-regcred -n gitlab-runner --docker-server=https://registry.gitlab.com --docker-username=<TOKEN NAME| USERNAME> --docker-password=<TOKEN>  -->

Create secret containing runner-token and runner-registration-token:

    # runnerRegistrationToken -- register a new runner
    # runnerToken -- register an existing runner
    microk8s.kubectl create secret generic gitlab-runner-secret -n gitlab-runner --from-literal=runner-registration-token=your-registration-token --from-literal=runner-token=""

Install GitLab Runner via helm3

    # helm install --namespace <NAMESPACE> gitlab-runner -f <CONFIG_VALUES_FILE> gitlab/gitlab-runner
    microk8s.helm3 install -n gitlab-runner gitlab-runner -f values.yaml gitlab/gitlab-runner

Upgrade GitLab Runner via helm3

    microk8s.helm3 upgrade gitlab-runner \
        --set gitlabUrl=http://gitlab.com,runnerRegistrationToken=your-registration-token \
        gitlab/gitlab-runner
